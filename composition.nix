{ pkgs, setup,  ... }: {

  nodes = {

    node = { pkgs, ... }: 
	{
      # add needed package
      environment.systemPackages = with pkgs; [ ior openmpi ];

      networking.firewall.enable = false;

 	environment.etc = {
      ior_script = {
        text = builtins.readFile ./script.ior;
      };
    };

      # Mount the NFS
      fileSystems."/data" = {
        device = "server:/";
        fsType = "nfs";
      };

    };

    server = { pkgs, ... }: {

      # Disable the firewall
      networking.firewall.enable = false;

      # Enable the nfs server services
      services.nfs.server.enable = true;

      # Define a mount point at /srv/shared
      services.nfs.server.exports = ''
        /srv/shared *(rw,no_subtree_check,fsid=0,no_root_squash)
      '';
      services.nfs.server.createMountPoints = true;

      services.nfs.server.nproc = setup.params.nfsNbProcs;

      # we also add the htop package for light monitoring
      environment.systemPackages = with pkgs; [ htop ];

    };
  };
  testScript = ''
    foo.succeed("true")
  '';
}
